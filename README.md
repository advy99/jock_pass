# Jockpass

Sometimes simple guessing in brute forcing makes close guessings but the target is slightly different. The idea of this tool is to poke around incorrect guessed passwords in case the target is similar but not exactly that one. 

It uses a **simple genetic algorithm** and saves the guessings of a **desired fitness** in an output file ready to be used in your favorite bruteforcing tool. 

![](https://media.giphy.com/media/20HHkJ3UpFZCt4nQwZ/giphy.gif)

## Compile and use:

```
$ g++ jockpass.cpp -o jockpass
$ ./jockpass {pass} {fitness}
```

For example:

```
$ ./guess PaS5w0Rd 1
$ cat candidates.txt
```
Would show something like this:

```
Generation: 128	String: PaS5w0Rd	Fitness: 0
...
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq

```
In case you have repeated options (like the one above) you can use the `clean_file.sh` script to remove duplicates with awk.

```
$ ./clean_file.sh
```
This has a [GNU](https://gitlab.com/terceranexus6/jock_pass/-/blob/master/LICENSE) licence.

Things I have in mind to enhance the tool:
* Pass a list of passwords instead of just one
* Better algorithm?
* Other languages

## Hacktoberfest 2021

¿Want to create a MR for [Hacktoberfest](https://hacktoberfest.digitalocean.com)? I created some issues you can check for help.

Any Merge Request will be taken in consideration, [here's](https://gitlab.com/terceranexus6/jock_pass/-/blob/master/example_of_merge_request.md) a template for making it easier for both you and me. 

FAQ:

**What is Hacktoberfest?**
A challenge to participate in open source projects in exchange of a limited edition t-shirt. Check more info [here](https://hacktoberfest.digitalocean.com).

**What is a merge request?**
Itś an improvement proposal for the project. The maintainer (the person responsible for the project) sometimes add "issues" when lack time, ability or resources for improving some things in the project. The community then read the issues (o come up with new ideas) and create a suggestion with the improvements in the Merge Request. Check about it [here](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html).

**What's and issue?**
An [issue](https://gitlab.com/terceranexus6/jock_pass/-/issues) is a "TODO" thing, a mistake or a bug. Whichever that needs improvements in the project. You can create one or read the ones already created.


If you have further questions, send them to inversealien@protonmail.com. 

![](https://hacktoberfest.digitalocean.com/_nuxt/img/logo-hacktoberfest-full.f42e3b1.svg)

